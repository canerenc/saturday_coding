﻿namespace ThermalHotel
{
    public class Dinner : IThermalHotel
    {
        private readonly IThermalHotel _thermalHotel;

        public Dinner(IThermalHotel thermalHotel)
        {
            _thermalHotel = thermalHotel;
        }

        public double GetPrice()
        {
            return _thermalHotel.GetPrice() + 15;
        }

        public string GetDescription()
        {
            return "Dinner " + _thermalHotel.GetDescription();
        }
    }
}