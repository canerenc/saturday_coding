﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightProvider.Content.Repository.Interface;
using FlightProvider.Content.Repository.Model;

namespace FlightProvider.Content.Repository
{
    public class AirportRepositoryWithCache:IAirportRepository
    {
        private readonly IAirportRepository _airportRepository;
        private Dictionary<string, Airport> airports; 
        public AirportRepositoryWithCache(IAirportRepository airportRepository)
        {
            _airportRepository = airportRepository;            
            FillDictionary();
        }

        private void FillDictionary()
        {
            airports = _airportRepository.GetAirports().ToDictionary(a => a.Code, a => a);
        }

        public IEnumerable<Airport> GetAirports()
        {
            if(airports==null)
                airports = _airportRepository.GetAirports().ToDictionary(a => a.Code, a => a);
            return airports.Values;
        }

        public IEnumerable<Airport> GetAirpotyLikeName(string name)
        {
            return airports.Values.Where(a => a.Name.IndexOf(name, StringComparison.CurrentCultureIgnoreCase)>=0);
        }

        public Airport GetByCode(string code)
        {

            return airports.ContainsKey(code) ? airports[code] : null;
        }

        public void Insert(Airport airport)
        {
            airports.Add(airport.Code,airport);
            _airportRepository.Insert(airport);
        }

        public void Update(Airport airport)
        {
            airports[airport.Code] = airport;
            _airportRepository.Update(airport);
        }

        public void Delete(string airportCode)
        {
            airports.Remove(airportCode);
            _airportRepository.Delete(airportCode);
        }

        public void CreateAirportTableIfNotExists()
        {
            _airportRepository.CreateAirportTableIfNotExists();
        }
    }
}