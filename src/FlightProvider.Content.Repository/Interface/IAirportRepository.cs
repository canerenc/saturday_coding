﻿using System.Collections.Generic;
using FlightProvider.Content.Repository.Model;

namespace FlightProvider.Content.Repository.Interface
{
    public interface IAirportRepository
    {
        IEnumerable<Airport> GetAirports();
        IEnumerable<Airport> GetAirpotyLikeName(string name);
        Airport GetByCode(string code);
        void Insert(Airport airport);
        void Update(Airport airport);
        void Delete(string airportCode);

        void CreateAirportTableIfNotExists();
    }
}
