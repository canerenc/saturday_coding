﻿using System;

namespace FlightSearch.Driver.Classes
{
    public class FlightSearchCriteria
    {
        public string FromCity { get; set; }
        public string ToCity { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
    }
}