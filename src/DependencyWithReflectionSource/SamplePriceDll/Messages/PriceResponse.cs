﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleMessagingModel;

namespace SamplePriceDll.Messages
{
    public class PriceResponse : Response
    {
        public int Id { get; set; }

        public string HotelName { get; set; }

        public string Currency { get; set; }

        public decimal Price { get; set; }
    }
}
