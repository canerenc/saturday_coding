﻿using SampleMessagingModel;

namespace SamplePriceDll.Messages
{
    [MessageRoute("price")]
    public class PriceRequest : IRequest<PriceResponse>
    {
        public int Id { get; set; }

        public string Location { get; set; }
    }
}
