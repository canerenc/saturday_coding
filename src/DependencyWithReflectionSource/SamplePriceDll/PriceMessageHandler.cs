﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleMessagingModel;
using SamplePriceDll.Messages;

namespace SamplePriceDll
{
    public class PriceMessageHandler : IServiceHandler<PriceRequest, PriceResponse>
    {
        public PriceResponse Retrieve(PriceRequest request)
        {
            var response = new PriceResponse { Id = request.Id, HotelName = "Falcon", Currency = "EUR", Price = 150.25m };
            return response;
        }
    }
}
