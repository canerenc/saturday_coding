﻿using System.Linq;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace SampleHotelDll
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var metadataProviderContributorsAssemblies = new[] { Assembly.GetExecutingAssembly() };

            container.Register(Classes.From(metadataProviderContributorsAssemblies.SelectMany(a => a.GetExportedTypes()))
                                            .Where(t => t.Name.EndsWith("Handler") && t.IsAbstract == false)
                                            .Configure(x => x.LifestyleSingleton())
            );
        }
    }
}
