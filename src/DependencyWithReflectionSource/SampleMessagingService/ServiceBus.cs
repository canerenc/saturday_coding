﻿using System;
using System.Linq;
using System.Reflection;
using Castle.MicroKernel;
using SampleMessagingModel;

namespace SampleMessagingService
{
    public class ServiceBus : IServiceBus
    {
        private readonly IKernel _container;
        public ServiceBus(IKernel container)
        {
            _container = container;
        }
        
        public Type GetQueryType(string method)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes()
                    .Where(type => Attribute.IsDefined(type, typeof (MessageRouteAttribute))
                                             &&
                                             type.GetCustomAttribute<MessageRouteAttribute>().Route == method
                    )
                ).SingleOrDefault();
        }

        public Response Send<TResponse>(IRequest<TResponse> request)
        {
            dynamic handler = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes()
                    .Where(type => typeof (IServiceHandler<,>).MakeGenericType(request.GetType(), typeof (TResponse))
                        .IsAssignableFrom(type) && !type.IsAbstract && !type.IsInterface)
                    .Select(i => _container.Resolve(i))
                ).Single();

            return handler.Retrieve((dynamic) request);
        }
    }
}
