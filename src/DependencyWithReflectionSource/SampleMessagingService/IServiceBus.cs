﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleMessagingModel;

namespace SampleMessagingService
{
    public interface IServiceBus
    {
        Type GetQueryType(string method);

        Response Send<TResponse>(IRequest<TResponse> request);
    }
}
