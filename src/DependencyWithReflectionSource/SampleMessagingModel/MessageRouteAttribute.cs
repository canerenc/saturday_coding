﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleMessagingModel
{
    public class MessageRouteAttribute : Attribute
    {
        public string Route { get; set; }
        public MessageRouteAttribute(string route)
        {
            Route = route;
        }
    }
}
