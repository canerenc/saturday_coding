﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiSampleUnitTests.Messages
{
    public class PriceResponse
    {
        public int Id { get; set; }

        public string HotelName { get; set; }

        public string Currency { get; set; }

        public decimal Price { get; set; }
    }
}
