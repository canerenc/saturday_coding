﻿using ApiSampleUnitTests.Messages;
using NUnit.Framework;
using FluentAssertions;
using RestSharp;

namespace ApiSampleUnitTests
{
    [TestFixture]
    public class PriceUnitTests
    {
        private const string Server = "http://localhost:9999/api";

        [Test]
        public void Get_Price()
        {
            var client = new RestClient(Server);
            var request = new RestRequest("price?Id=10", Method.GET)
            {
                RequestFormat = DataFormat.Json
            };

            var response = client.Execute<PriceResponse>(request);
            var responseData = response.Data;

            response.ContentType.Should().Be("application/json; charset=utf-8");

            responseData.Id.Should().Be(10);
            responseData.HotelName.Should().Be("Falcon");
            responseData.Currency.Should().Be("EUR");
            responseData.Price.Should().Be(150.25m);
        }

        [Test]
        public void Post_Price()
        {
            var client = new RestClient(Server);
            var request = new RestRequest("price", Method.POST)
            {
                RequestFormat = DataFormat.Json
            };

            var model = new PriceRequest
            {
                Id = 10
            };

            request.AddBody(model);

            var response = client.Execute<PriceResponse>(request);
            var responseData = response.Data;

            response.ContentType.Should().Be("application/json; charset=utf-8");

            responseData.Id.Should().Be(model.Id);
            responseData.HotelName.Should().Be("Falcon");
            responseData.Currency.Should().Be("EUR");
            responseData.Price.Should().Be(150.25m);
        }
    }
}
