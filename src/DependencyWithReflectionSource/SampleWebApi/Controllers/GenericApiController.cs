﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SampleMessagingModel;
using SampleMessagingService;
using SampleWebApi.Extensions;

namespace SampleWebApi.Controllers
{
    public class GenericApiController : ApiController
    {
        private readonly IServiceBus _serviceBus;

        public GenericApiController(IServiceBus serviceBus)
        {
            _serviceBus = serviceBus;
        }

        public HttpResponseMessage Get(string method, HttpRequestMessage request)
        {
            return FindService(method, request);
        }

        public HttpResponseMessage Post([FromUri] string method, HttpRequestMessage request)
        {
            return FindService(method, request);
        }

        private HttpResponseMessage FindService(string method, HttpRequestMessage request)
        {
            var requestType = _serviceBus.GetQueryType(method);

            var model = request.DeserializeRequestMessage(requestType);
            Response result = _serviceBus.Send(model);
            var response = request.CreateResponse(HttpStatusCode.OK, result);
            return response;
        }
    }
}
