﻿using System;
using System.CodeDom.Compiler;
using System.Globalization;
using System.Security;
using System.Xml.Schema;
using System.Xml.Serialization;
using T4Generator;

namespace CodeTemplates
{
    public class Test
    {
    }

    public class UserBuilder
    {
        public UserBuilder User
        {
            get { return new UserBuilder(); }
        }

        string m_name;

        public UserBuilder Name(string value)
        {
            this.m_name = value;
            return this;
        }

        string m_lastName;

        public UserBuilder LastName(string value)
        {
            this.m_lastName = value;
            return this;
        }

        int m_age;

        public UserBuilder Age(int value)
        {
            this.m_age = value;
            return this;
        }

        public User Build()
        {
            Validate();
            throw new System.NotImplementedException();
            return new User
            {
                
            };
        }

        private void Validate()
        {
            throw new System.NotImplementedException();
        }
    }

    public class XmlExample
    {

        [XmlIgnore]
        public DateTime Checkin { get; set; }

        [XmlElement(ElementName = "Checkin")]
        public string ProxyCheckin
        {
            get { return Checkin.ToString("dd.MM.yyyy", new CultureInfo("en-US")); }
            set { Checkin = DateTime.ParseExact(value, "dd.MM.yyyy", CultureInfo.InvariantCulture); }
        }

        [XmlIgnore]
        public Language? Language { get; set; }

        [XmlElement(ElementName = "Language")]
        public int? ProxyLanguage
        {
            get { return Language.HasValue ? (int) Language.Value : (int?) null; }
            set
            {
                if (value != null)
                {
                    Language = (Language) value;
                }
            }
        }

    }
}