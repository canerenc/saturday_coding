﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace SignalR.Hubs
{
 

    public class GameHub : Hub
    {
        private static int clientCounter = 0;
        public void SendPlayedObject(string id)
        {
            Clients.Others.PushPlayedObject(id, Context.ConnectionId);
        }
        public void ConnectedClients()
        {
            Clients.Caller.ClientCount(clientCounter);
        }
        public override Task OnConnected()
        {
            clientCounter++;
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            clientCounter--;
            return base.OnDisconnected();
        }
    }
}