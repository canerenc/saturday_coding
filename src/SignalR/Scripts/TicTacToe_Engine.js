﻿var attName = "Status";
var x_ImagePath = '../Images/X.png';
var o_ImagePath = '../Images/O.png';
var currentImage = x_ImagePath;

function CheckGameResult(obj) {
    obj.css("background-image", "url(" + currentImage + ")");
    if (currentImage == x_ImagePath) {
        obj.attr("status", "X");
        currentImage = o_ImagePath;
    }
    else {
        obj.attr("status", "O");
        currentImage = x_ImagePath;
    }

    var values = $("#cell11").attr(attName) + $("#cell12").attr(attName) + $("#cell13").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell11").css("background-color", "gainsboro");
        $("#cell12").css("background-color", "gainsboro");
        $("#cell13").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell21").attr(attName) + $("#cell22").attr(attName) + $("#cell23").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell21").css("background-color", "gainsboro");
        $("#cell22").css("background-color", "gainsboro");
        $("#cell23").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell31").attr(attName) + $("#cell32").attr(attName) + $("#cell33").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell31").css("background-color", "gainsboro");
        $("#cell32").css("background-color", "gainsboro");
        $("#cell33").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell11").attr(attName) + $("#cell22").attr(attName) + $("#cell33").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell11").css("background-color", "gainsboro");
        $("#cell22").css("background-color", "gainsboro");
        $("#cell33").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell13").attr(attName) + $("#cell22").attr(attName) + $("#cell31").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell13").css("background-color", "gainsboro");
        $("#cell22").css("background-color", "gainsboro");
        $("#cell31").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell11").attr(attName) + $("#cell21").attr(attName) + $("#cell31").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell11").css("background-color", "gainsboro");
        $("#cell21").css("background-color", "gainsboro");
        $("#cell31").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell12").attr(attName) + $("#cell22").attr(attName) + $("#cell32").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell12").css("background-color", "gainsboro");
        $("#cell22").css("background-color", "gainsboro");
        $("#cell32").css("background-color", "gainsboro");
        return true;
    }
    values = $("#cell13").attr(attName) + $("#cell23").attr(attName) + $("#cell33").attr(attName);
    if (CheckIt(values) == true)
    {
        $("#cell13").css("background-color", "gainsboro");
        $("#cell23").css("background-color", "gainsboro");
        $("#cell33").css("background-color", "gainsboro");
        return true;
    }
    return false;

}

function CheckIt(values) {
    if (values == "OOO" || values == "XXX")
        return true;
    else {
        return false;
    }
}